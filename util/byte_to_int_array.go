// Copyright 2022 Tobias Berdin

package util

// ToIntArray translates a byte array into an array of integers.
func ToIntArray(b []byte) []int {
	ret := make([]int, len(b))
	for i, v := range b {
		result := int(v)
		ret[i] = result
	}
	return ret
}
