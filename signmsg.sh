#!/bin/sh

# check if args exist
if [ -z "$1" ] || [ -z "$2" ]; then
  exit 1
fi

TEMP_FOLDER=$1 # /var/lib/epid/<nonce>/
MSG=$2

# execute signmsg binary
signmsg --msg="$MSG" --sig="${TEMP_FOLDER}sig.dat" --gpubkey="${TEMP_FOLDER}pubkey.bin" --mprivkey="${TEMP_FOLDER}mprivkey.dat" --capubkey="/var/lib/epid/cacert.bin"
