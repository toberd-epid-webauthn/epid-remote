// Copyright 2022 Tobias Berdin

package request

import (
	"encoding/json"
	"epid-remote/core"
	"fmt"
	"github.com/spf13/viper"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"
)

func Verifysig(w http.ResponseWriter, r *http.Request) {
	// add cors header to all responses
	w.Header().Add("Access-Control-Allow-Origin", "*")

	// ensure that request is POST
	if r.Method != "POST" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	// read request body
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Println("error while reading request: ", err.Error())
		http.Error(w, "Internal server error: error while reading request", http.StatusInternalServerError)
		return
	}

	// close request body reader
	err = r.Body.Close()
	if err != nil {
		fmt.Println("error while closing request body: ", err.Error())
		http.Error(w, "Internal server error: error while closing request body", http.StatusInternalServerError)
		return
	}

	if len(body) == 0 {
		http.Error(w, "Bad request: empty body", http.StatusBadRequest)
		return
	}

	// parse request body into core.VerifysigRequest
	request := core.VerifysigRequest{}
	err = json.Unmarshal(body, &request)
	if err != nil {
		fmt.Println("error during json unmarshal: ", err.Error())
		http.Error(w, "Internal server error: error while reading request body", http.StatusInternalServerError)
		return
	}

	// create a unique temporary folder
	tempFolder := "/var/lib/epid/" + strconv.FormatInt(time.Now().UnixNano(), 10) + "/"

	// create temp dir using unique timestamp
	_, err = exec.Command("mkdir", tempFolder).Output()
	if err != nil {
		fmt.Println("error while creating temp folder: ", err.Error())
		http.Error(w, "Internal server error: error while verifying signature", http.StatusInternalServerError)
		return
	}

	// remote temp folder once the function returns
	defer func(path string) {
		err := os.RemoveAll(path)
		if err != nil {
			fmt.Println("error while removing temp folder: ", err.Error())
		}
	}(tempFolder)

	// create file for the signature
	f, err := os.Create(tempFolder + "sig.dat")
	if err != nil {
		fmt.Println("error while creating sig.dat file: ", err.Error())
		http.Error(w, "Internal server error: error while verifying signature", http.StatusInternalServerError)
		return
	}

	// write the signature to the created file
	_, err = f.Write(request.Signature)
	if err != nil {
		fmt.Println("error while writing sig.dat file: ", err.Error())
		http.Error(w, "Internal server error: error while verifying signature", http.StatusInternalServerError)
		// close file
		f.Close()
		return
	}

	// close file
	f.Close()

	// create file for the group public key
	f, err = os.Create(tempFolder + "pubkey.bin")
	if err != nil {
		fmt.Println("error while creating pubkey.bin file: ", err.Error())
		http.Error(w, "Internal server error: error while signing message", http.StatusInternalServerError)
		return
	}

	// write the public key to the created file
	_, err = f.Write(request.PublicKey)
	if err != nil {
		fmt.Println("error while writing pubkey.bin file: ", err.Error())
		http.Error(w, "Internal server error: error while signing message", http.StatusInternalServerError)
		// close file
		f.Close()
		return
	}

	// close file
	f.Close()

	verifysigSH := viper.GetString("projectRoot") + "verifysig.sh"

	// execute verifysig.sh script
	out, err := exec.Command(verifysigSH, tempFolder, request.Message).Output()
	if err != nil {
		fmt.Println("error while verifying message: ", err.Error())
		http.Error(w, "invalid", http.StatusInternalServerError)
		return
	}

	// check if output contains an error
	if !strings.Contains(string(out), "signature verified successfully") {
		fmt.Println("error while verifying message: ", string(out))
		http.Error(w, "invalid", http.StatusInternalServerError)
		return
	}

	// send response to client
	_, err = fmt.Fprint(w, "valid")
	if err != nil {
		fmt.Println("error while sending response: ", err.Error())
		http.Error(w, "Internal server error: error while sending response", http.StatusInternalServerError)
	}
	fmt.Println("Signature verified")
}
