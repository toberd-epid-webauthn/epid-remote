// Copyright 2022 Tobias Berdin

package request

import (
	"encoding/json"
	"epid-remote/core"
	"epid-remote/util"
	"fmt"
	"github.com/spf13/viper"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"time"
)

func Signmsg(w http.ResponseWriter, r *http.Request) {
	// add cors header to all responses
	w.Header().Add("Access-Control-Allow-Origin", "*")

	// ensure that request is POST
	if r.Method != "POST" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	// read request body
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Println("error while reading request: ", err.Error())
		http.Error(w, "Internal server error: error while reading request", http.StatusInternalServerError)
		return
	}

	// close request body reader
	err = r.Body.Close()
	if err != nil {
		fmt.Println("error while closing request body: ", err.Error())
		http.Error(w, "Internal server error: error while closing request body", http.StatusInternalServerError)
		return
	}

	if len(body) == 0 {
		http.Error(w, "Bad request: empty body", http.StatusBadRequest)
		return
	}

	// parse request body into core.SignmsgRequest
	request := core.SignmsgRequest{}
	err = json.Unmarshal(body, &request)
	if err != nil {
		fmt.Println("error during json unmarshal: ", err.Error())
		http.Error(w, "Internal server error: error while reading request body", http.StatusInternalServerError)
		return
	}

	// create a unique temporary folder
	tempFolder := "/var/lib/epid/" + strconv.FormatInt(time.Now().UnixNano(), 10) + "/"

	// create temp dir using unique timestamp
	_, err = exec.Command("mkdir", tempFolder).Output()
	if err != nil {
		fmt.Println("error while creating temp folder file: ", err.Error())
		http.Error(w, "Internal server error: error while signing message", http.StatusInternalServerError)
		return
	}

	// remote temp folder once the function returns
	defer func(path string) {
		err := os.RemoveAll(path)
		if err != nil {
			fmt.Println("error while removing temp folder: ", err.Error())
		}
	}(tempFolder)

	// create file for the member private key
	f, err := os.Create(tempFolder + "mprivkey.dat")
	if err != nil {
		fmt.Println("error while creating mprivkey.dat file: ", err.Error())
		http.Error(w, "Internal server error: error while signing message", http.StatusInternalServerError)
		return
	}

	// write the private key to the created file
	_, err = f.Write(request.PrivateKey)
	if err != nil {
		fmt.Println("error while writing mprivkey.dat file: ", err.Error())
		http.Error(w, "Internal server error: error while signing message", http.StatusInternalServerError)
		// close file
		f.Close()
		return
	}

	// close file
	f.Close()

	// create file for the group public key
	f, err = os.Create(tempFolder + "pubkey.bin")
	if err != nil {
		fmt.Println("error while creating pubkey.bin file: ", err.Error())
		http.Error(w, "Internal server error: error while signing message", http.StatusInternalServerError)
		return
	}

	// write the public key to the created file
	_, err = f.Write(request.PublicKey)
	if err != nil {
		fmt.Println("error while writing pubkey.bin file: ", err.Error())
		http.Error(w, "Internal server error: error while signing message", http.StatusInternalServerError)
		// close file
		f.Close()
		return
	}

	// close file
	f.Close()

	signmsgSH := viper.GetString("projectRoot") + "signmsg.sh"

	// execute signmsg.sh script
	out, err := exec.Command(signmsgSH, tempFolder, request.Message).Output()
	if err != nil {
		fmt.Println("error while signing message: ", err.Error())
		http.Error(w, "Internal server error: error while signing message", http.StatusInternalServerError)
		return
	}

	// check if output is not empty
	if len(out) != 0 {
		fmt.Println("error while signing message: ", out)
		http.Error(w, "Internal server error: error while signing message", http.StatusInternalServerError)
		return
	}

	// success
	f, err = os.Open(tempFolder + "sig.dat")
	if err != nil {
		fmt.Println("error while opening sig.dat file: ", err.Error())
		http.Error(w, "Internal server error: error while returning signature", http.StatusInternalServerError)
		return
	}

	buf := make([]byte, 512)

	n, err := f.Read(buf)
	if err != nil {
		fmt.Println("error while reading sig.dat file: ", err.Error())
		http.Error(w, "Internal server error: error while returning signature", http.StatusInternalServerError)
		// close file
		f.Close()
		return
	}

	// close file
	f.Close()

	// create response and convert it to JSON
	responseStruct := core.SignmsgResponse{Signature: util.ToIntArray(buf[:n])}
	response, err := json.Marshal(responseStruct)
	if err != nil {
		fmt.Println("error during json marshal: ", err.Error())
		http.Error(w, "Error while creating response body", http.StatusInternalServerError)
		return
	}

	// send response to client
	_, err = fmt.Fprint(w, string(response))
	if err != nil {
		fmt.Println("error while sending response: ", err.Error())
		http.Error(w, "Internal server error: error while sending response", http.StatusInternalServerError)
	}
	fmt.Println("Message signed")
}
