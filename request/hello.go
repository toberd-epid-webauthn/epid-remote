// Copyright 2022 Tobias Berdin

package request

import (
	"fmt"
	"net/http"
)

func Hello(w http.ResponseWriter, r *http.Request) {
	_, err := fmt.Fprintf(w, "Hello")
	if err != nil {
		fmt.Println(err.Error())
	}
}
