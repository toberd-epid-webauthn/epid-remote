#!/bin/sh

# check if args exist
if [ -z "$1" ] || [ -z "$2" ]; then
  exit 1
fi

TEMP_FOLDER=$1 # /var/lib/epid/<nonce>/
MSG=$2

# execute verifysig binary
verifysig --msg="$MSG" --sig="${TEMP_FOLDER}sig.dat" --gpubkey="${TEMP_FOLDER}pubkey.bin" --capubkey="/var/lib/epid/cacert.bin" --grprl="/var/lib/epid/grprl.bin"
