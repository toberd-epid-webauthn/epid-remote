#
# Build stage
#
FROM golang:1.16 AS build

WORKDIR /go/src/epid-remote
COPY . .

# Download go dependencies
RUN go mod download

# Build epid-remote binary
RUN go build -v -o epid-remote .

#
# Deploy stage
#
FROM ubuntu:latest AS deploy

WORKDIR /go/src/epid-remote

# Copy build binary
COPY --from=build /go/src/epid-remote/epid-remote .

# Copy shell scripts
COPY --from=build /go/src/epid-remote/*.sh .

# Copy config file
COPY --from=build /go/src/epid-remote/config/config.yaml ./config/

# Copy EPID binaries and make them executable
COPY --from=build /go/src/epid-remote/signmsg /usr/bin
RUN chmod +x /usr/bin/signmsg
COPY --from=build /go/src/epid-remote/verifysig /usr/bin
RUN chmod +x /usr/bin/verifysig

# Copy default EPID data
COPY --from=build /go/src/epid-remote/*.bin /var/lib/epid/

# Run epid-remote API
EXPOSE 8080
CMD [ "./epid-remote" ]