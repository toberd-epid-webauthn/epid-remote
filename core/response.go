// Copyright 2022 Tobias Berdin

package core

// SignmsgResponse contains the signature of the signed message.
//
// Signature is represented as integer array so that json marshal keeps the
// structure of a list, not a base64 encoded string.
type SignmsgResponse struct {
	Signature []int `json:"signature"`
}
