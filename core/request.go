// Copyright 2022 Tobias Berdin

package core

// SignmsgRequest is used when a client wants to sign a message with its
// private member key.
type SignmsgRequest struct {
	PublicKey  []byte `json:"publicKey"`
	PrivateKey []byte `json:"privateKey"`
	Message    string `json:"message"`
}

// VerifysigRequest is used when a relying party wants to verify a signature.
type VerifysigRequest struct {
	PublicKey []byte `json:"publicKey"`
	Signature []byte `json:"signature"`
	Message   string `json:"message"`
}
