// Copyright 2022 Tobias Berdin

package main

import (
	"epid-remote/request"
	"fmt"
	"github.com/spf13/viper"
	"net/http"
	"os"
)

// main is the entry function for the API.
func main() {
	// initialize config
	initConfig()

	// register API endpoints
	http.HandleFunc("/", request.Hello)
	http.HandleFunc("/signmsg", request.Signmsg)
	http.HandleFunc("/verifysig", request.Verifysig)

	port := viper.GetInt("port")

	fmt.Printf("Listening on port %d ...\n", port)
	// start server on the specified port
	err := http.ListenAndServe(fmt.Sprintf(":%d", port), nil)
	if err != nil {
		fmt.Println(err.Error())
	}
	fmt.Println("Finished")
}

// initConfig registers the config file.
func initConfig() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("./config/")

	err := viper.ReadInConfig()
	if err != nil {
		fmt.Println("fatal error config file: default \n", err)
		os.Exit(1)
	}
}
